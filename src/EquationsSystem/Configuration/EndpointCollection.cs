﻿using System.Configuration;

namespace EquationsSystem.Configuration
{

    [ConfigurationCollection(typeof(EndpointElement), AddItemName = "endpoint",
    CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class EndpointCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement () {
            return new EndpointElement();
        }

        protected override object GetElementKey (ConfigurationElement element) {
            return ((EndpointElement) (element)).Address;
        }
    }
}
