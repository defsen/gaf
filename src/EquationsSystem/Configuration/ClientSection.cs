﻿using System.Configuration;

namespace EquationsSystem.Configuration
{
    public class ClientSection : ConfigurationSection
    {
        ///// <summary>
        ///// 
        ///// </summary>
        [ConfigurationProperty("fitness", IsKey = false, IsRequired = false)]
        public FitnessElement Fitness
        {
			get { return (FitnessElement)base["fitness"]; }
        }

        /// <summary>
        /// Gets the servicediscovery element.
        /// </summary>
        /// <value>The service discovery.</value>
        // [ConfigurationProperty ("serviceDiscovery", IsKey = false, IsRequired = false)]
        // public ServiceDiscoveryElement ServiceDiscovery {
        // 	get { return (ServiceDiscoveryElement)base ["serviceDiscovery"]; }
        //}

        //[ConfigurationProperty("dataAccessLayer")]
        //public DataAccessLayer DataAccessLayer
        //{
        //    get { return (DataAccessLayer)base["dataAccessLayer"]; }

        //}

        //        /// <summary>
        //        /// A collection of the configured themes
        //        /// </summary>
        //        [ConfigurationProperty("sites")]
        //        public SiteCollection Sites
        //        {
        //            get { return ((SiteCollection)(base["sites"])); }
        //        }

        [ConfigurationProperty("endpoints")]
        public EndpointCollection Endpoints {
            get { return ((EndpointCollection) (base["endpoints"])); }
        }

        [ConfigurationProperty("settings")]
		public SettingCollection Settings
		{
			get { return ((SettingCollection)(base["settings"])); }
		}

    }
}
