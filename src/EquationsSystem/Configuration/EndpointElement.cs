﻿using System.Configuration;

namespace EquationsSystem.Configuration
{
    public class EndpointElement : ConfigurationElement
    {
        [ConfigurationProperty("address", IsRequired = true)]
        public string Address {
            get { return (string) base["address"]; }
        }
    }
}
