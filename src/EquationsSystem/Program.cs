﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net;

using GAF;
using GAF.Network;
using GAF.REST;
using GAF.Operators;

namespace EquationsSystem
{
    public class Program
    {
        private static Stopwatch _stopWatch;
        private const int populationSize = 100;
        private const int chromosomeLength = 44;

        private static void Main(string[] args)
        {
            var population = new Population(populationSize, chromosomeLength, false, true);

            //create the elite operator 
            var elite = new Elite(5);

            //create crossover operator 
            var crossover = new Crossover(0.85) { CrossoverType = CrossoverType.SinglePoint };

            //create the BinaryMutate operator 
            var mutate = new BinaryMutate(0.05);

            //note that for network fitness evaluation we simply pass null instead of a fitness
            //function.
            var ga = new GeneticAlgorithm(population, null);

            //subscribe to the generation and run complete events 
            ga.OnGenerationComplete += ga_OnGenerationComplete;
            ga.OnRunComplete += ga_OnGenerationComplete;

            //add the operators 
            ga.Operators.Add(elite);
            ga.Operators.Add(crossover);
            ga.Operators.Add(mutate);
            
            bool useRest = true;

            if (useRest)
            {
                var endpoints = new List<Uri>();
                foreach (var e in Configuration.ConfigurationManager.Client.Endpoints)
                {
                    endpoints.Add(new Uri((e as Configuration.EndpointElement).Address));
                }

                var networkWrapper = new RESTWrapper(ga, endpoints, Configuration.ConfigurationManager.Client.Fitness.AssemblyName, false);

                _stopWatch = new Stopwatch();
                _stopWatch.Start();

                //locally declared terminate function
                networkWrapper.GeneticAlgorithm.Run(TerminateAlgorithm);
            }
            else
            {
                var endpoints = new List<IPEndPoint>();
                foreach (var e in Configuration.ConfigurationManager.Client.Endpoints)
                {
                    endpoints.Add(NetworkWrapper.CreateEndpoint((e as Configuration.EndpointElement).Address));
                }

                IServiceDiscovery serviceDiscovery = new GAF.ServiceDiscovery.ServiceEndpoints(endpoints);

                var networkWrapper = new NetworkWrapper(ga, serviceDiscovery, Configuration.ConfigurationManager.Client.Fitness.AssemblyName, true);

                _stopWatch = new Stopwatch();
                _stopWatch.Start();

                //locally declared terminate function
                networkWrapper.GeneticAlgorithm.Run(TerminateAlgorithm);
            }

            Console.ReadLine();
        }

        private static bool TerminateAlgorithm(Population population, int currentGeneration, long currentEvaluation)
        {
            return currentGeneration >= 350;
        }

        private static void ga_OnGenerationComplete(object sender, GaEventArgs e)
        {
            var chromosome = e.Population.GetTop(1)[0];

            var rangeConst = GAF.Math.GetRangeConstant(100, chromosome.Count / 2);

            var x1 = rangeConst * Convert.ToInt32(chromosome.ToBinaryString(0, chromosome.Count / 2), 2) - 50;
            var x2 = rangeConst * Convert.ToInt32(chromosome.ToBinaryString(chromosome.Count / 2, chromosome.Count / 2), 2) - 50;

            Console.WriteLine(String.Format("Generation: {0}, Evaluations: {1}, Fitness: {2}, x1: {3}, x2: {4}, ElapsedTime: {5}ms",
                e.Generation,
                e.Evaluations,
                chromosome.Fitness,
                x1, x2,
                _stopWatch.ElapsedMilliseconds)
            );

            _stopWatch.Restart();
        }
    }
}
