﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using GAF;
using GAF.Network;

namespace EquationsSystem.RemoteFitness
{
    public class EquationsSystem : GAF.Network.IRemoteFitness
    {
        public double EvaluateFitness(Chromosome chromosome)
        {
            var range = GAF.Math.GetRangeConstant(100, chromosome.Count / 2);

            var x1 = (Convert.ToInt32(chromosome.ToBinaryString(0, chromosome.Count / 2), 2) * range) - 50;
            var x2 = (Convert.ToInt32(chromosome.ToBinaryString(chromosome.Count / 2, chromosome.Count / 2), 2) * range) - 50;

            var temp1 = x1 + x2 - 5;
            var temp2 = x1 * x1 - x2 - 1;

            return 1 - (Fit(temp1) + Fit(temp2)) / 2;
        }

        public List<Type> GetKnownTypes()
        {
            return null;
        }

        private static double Fit(double val)
        {
            return System.Math.Abs(System.Math.Atan(val) / System.Math.PI / 2);
            // return System.Math.Abs((1d / (1 + System.Math.Exp(-val))) * 2 - 1);
        }
    }
}
