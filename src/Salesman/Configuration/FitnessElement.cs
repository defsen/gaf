﻿using System.Configuration;

namespace Salesman.Configuration
{
    public class FitnessElement : ConfigurationElement
    {
        [ConfigurationProperty("assemblyName", IsKey = true, IsRequired = false)]
        public string AssemblyName
        {
            get { return (string)base["assemblyName"]; }
        }

    }
}
