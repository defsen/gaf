﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace GAF.REST
{
    [DataContract]
    public class Response
    {
        public Response(double result) {
            Result = result;
        }

        [DataMember]
        public double Result { internal set; get; }
    }
}
