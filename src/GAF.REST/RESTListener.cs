﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using GAF;
using GAF.Network;
using GAF.Network.Serialization;
using System.Threading;
using System.IO;

namespace GAF.REST
{
    public class RESTListener
    {
        public delegate void RequestReceivedHandler (object sender, RequestEventArgs e);

        public static event RequestReceivedHandler OnRequestReceived;

        public static void Start (string prefix, int accept = 4) {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add(prefix + "evaluate/");
            listener.IgnoreWriteExceptions = true;

            var semaphore = new Semaphore(accept, accept);

            listener.Start();

            while (true) {
                semaphore.WaitOne();

                listener.GetContextAsync().ContinueWith(async (contextTask) => {
                    try {
                        semaphore.Release();

                        Log.Debug("Accepted connection in Thread " + Thread.CurrentThread.ManagedThreadId);
                        await ProcessListenerContext(await contextTask);
                    } catch (Exception ex) {
                        throw ex;
                    }
                });

                // listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);
                // _allDone.WaitOne();
            }
        }

        public static async Task ProcessListenerContext(HttpListenerContext context) {
            // HttpListener listener = (HttpListener) result.AsyncState;

            HttpListenerRequest request = context.Request;
            HttpListenerResponse response = context.Response;
            
            Log.Debug(string.Format("Request Received, Method {0}, ThreadId {1}, Path {2}", 
                request.HttpMethod, 
                Thread.CurrentThread.ManagedThreadId,
                request.RawUrl));

            if (request.HttpMethod.ToLower() != "post") {
                response.StatusCode = (int) HttpStatusCode.MethodNotAllowed;
                response.OutputStream.Close();
                return;
            }

            if (request.RawUrl.StartsWith("/evaluate")) {
                if (OnRequestReceived != null) {
                    byte[] recData = new byte[request.ContentLength64];

                    using (BinaryReader reader = new BinaryReader(request.InputStream))
                        recData = reader.ReadBytes((int) request.ContentLength64);

                    var args = new RequestEventArgs(RequestId.Evaluate, recData);
                    await Task.Factory.StartNew(() => OnRequestReceived(null, args));

                    var xmitData = Binary.Serialize(new Response(args.Result));

                    response.ContentLength64 = xmitData.Length;
                    response.OutputStream.Write(xmitData, 0, xmitData.Length);
                    response.OutputStream.Close();
                }
            }
        }
    }
}
