﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GAF.REST
{
    public class RequestEventArgs
    {
        private readonly RequestId _id;
        private readonly byte[] _data;
        
        /// <summary>
		/// Constructor.
		/// </summary>
		/// <param name = "data"></param>
		public RequestEventArgs (RequestId id, byte[] data) {
            _id = id;
            _data = data;
        }

        /// <summary>
		/// Returns the Data associated with this event.
		/// </summary>
		/// <value>The header.</value>
		public byte[] Data {
            get { return _data; }
        }        

        public RequestId RequestId { 
            get { return _id; }
        }

        /// <summary>
        /// Used to return a result from the event code.
        /// </summary>
        /// <value>The fitness.</value>
        public double Result { set; get; }
    }
}
