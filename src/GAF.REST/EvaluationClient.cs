﻿/*
	Genetic Algorithm Framework for .Net
	Copyright (C) 2016  John Newcombe

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

		You should have received a copy of the GNU Lesser General Public License
		along with this program.  If not, see <http://www.gnu.org/licenses/>.

	http://johnnewcombe.net
*/


using System.Text;
using System;
using System.Net;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using System.IO;
using System.Diagnostics;
using GAF.Network.Serialization;
using System.Collections;
using GAF.Network.Threading;
using GAF.Network;
using System.Net.Http;
using System.Globalization;

namespace GAF.REST
{
    /// <summary>
    /// Evaluation client.
    /// </summary>
    public class EvaluationClient : IDisposable
    {
        private FitnessAssembly _fitnessAssembly;
        private string _fitnessAssemblyName;
        private BlockingCollection<HttpClient> _clientPool;
        private GAF.Network.Threading.ProducerConsumerQueue _pcQueue;

        /// <summary>
        /// Initializes a new instance of the <see cref="GAF.Net.EvaluationClient"/> class.
        /// </summary>
        /// <param name="endPoints">End points.</param>
        public EvaluationClient (List<Uri> endPoints, string fitnessAssemblyName) {
            if (endPoints == null) {
                throw new ArgumentNullException(nameof(endPoints), "The parameter is null.");
            }

            if (string.IsNullOrWhiteSpace(fitnessAssemblyName)) {
                throw new ArgumentException("The specified fitness assembly name is null or empty.", nameof(fitnessAssemblyName));
            }

            _clientPool = new BlockingCollection<HttpClient>();
            foreach (var uri in endPoints)
                _clientPool.Add(RESTClient.Create(uri));

            _pcQueue = new GAF.Network.Threading.ProducerConsumerQueue(endPoints.Count);

            _fitnessAssemblyName = fitnessAssemblyName;
            _fitnessAssembly = new FitnessAssembly(fitnessAssemblyName);

            // InitialiseServers (true);
        }


        /// <summary>
        /// Replaces existing endpoints with specified endpoints.
        /// </summary>
        /// <param name="endpoints">Endpoints.</param>
        public void UpdateEndpoints (List<IPEndPoint> endpoints) {
            //TODO: Implement me
            //bear in mind that sockets will be constantly changing
            //can this even work?
        }

        private void InitialiseServers (bool forceInitialisation) {
            //send a status packet to see if we have already initialised this connection
            //i.e. passed the fitness function accross
            /* try {

				// var socketPoolItems = _socketPool.Sockets;
				foreach (var clint in _clientPool) {

					//TODO: Check that item is connected.
					var serverStatus = GetServerStatus (socketPoolItem.Socket);

					//check if it is ok to init server with fitness etc
					if (!serverStatus.ServerDefinedFitness && (!serverStatus.Initialised || forceInitialisation)) {

						Log.Info (string.Format ("Sending the fitness function to server {0}.", socketPoolItem.EndPoint));

						var functionBytes = File.ReadAllBytes (_fitnessAssemblyName);

						//send to server
						var xmitPacket = new Packet (functionBytes, PacketId.Init, Guid.Empty);
						SocketClient.TransmitData (socketPoolItem.Socket, xmitPacket);
					}
				}

			} catch (Exception ex) {
				Log.Error (ex);
			} */
        }

        /// <summary>
        /// Evaluate the specified solutionsToEvaluate.
        /// </summary>
        /// <param name="solutionsToEvaluate">Solutions to evaluate.</param>
        public async Task<int> Evaluate (List<Chromosome> solutionsToEvaluate) {
            //this method is called after each operator, the solutionsToEvaluate list
            //contains all the required for a complete generation.
            try {

                if (solutionsToEvaluate == null) {
                    throw new ArgumentNullException(nameof(solutionsToEvaluate), "The parameter is null.");
                }

                var solutionCount = solutionsToEvaluate.Count;
                if (solutionCount == 0) {
                    throw new ArgumentException("The parameter is empty.", nameof(solutionsToEvaluate));
                }

                //create a concurrent queue with max concurrency equal 
                //to the endpoint count and add the delegates to the queue
                foreach (var solution in solutionsToEvaluate) {
                    var chromosome = solution;
                    var task = _pcQueue.Enqueue(() => Evaluate(chromosome));
                    Log.Debug(string.Format("Chromosome [{0}] evaluation queued as Task {1} queued.", chromosome.Id, task.Id));
                }

                var allTasks = Task.WhenAll(_pcQueue.AllTasks);
                await allTasks;

                // set the number of evaluations we have done (one per solution)
                return solutionCount;
            }
            catch (Exception) {
                throw;
            }

        }

        private ServerStatus GetServerStatus (HttpClient client) {
            /* var statusRequestPacket = new Packet (PacketId.Status);
			var statusPacket = SocketClient.TransmitData (client, statusRequestPacket);

			//check the status packet and decode with the ServerStatus class.
			if (statusPacket == null) {
				throw new GAF.Network.SocketException ("Status Packet was not received or was empty.");
			}

			return new ServerStatus (statusPacket); */

            return null;
        }

        private void Evaluate (Chromosome chromosome) {
            HttpClient client = null;

            try {
                client = _clientPool.Take();
                Log.Debug(string.Format("Client: {0} retrieved from the pool.", client.BaseAddress));

                chromosome.Tag = client;
                chromosome.Evaluate(RemoteFitnessDelegateFunction);
            } catch (Exception ex) {

                if (client != null) {
                    Log.Error(string.Format("{0} [Client:{1}]", ex.Message, client.BaseAddress));
                }
                else {
                    Log.Error(ex);
                }

                var task = _pcQueue.Enqueue(() => Evaluate(chromosome));
                Log.Debug(string.Format("Chromosome [{0}] evaluation re-queued as Task {1} queued.", chromosome.Id, task.Id));

                throw;

            } finally {
                //finished with the endpoint so pop it back in the list to be used by this or another worker
                _clientPool.Add(client);
            }
        }

        private double RemoteFitnessDelegateFunction (Chromosome chromosome) {
            double fitness = 0.0;

            //retrieve the client to be used;
            var client = (HttpClient) chromosome.Tag;

            // Convert the passed chromosome to a byte array
            //actually just the genes are serialised as we dont need to sent the rest
            //var xmitData = Serializer.Serialize<Chromosome> (chromosome, _fitnessAssembly.KnownTypes);
            var xmitData = Binary.Serialize<List<Gene>>(chromosome.Genes, _fitnessAssembly.KnownTypes);
            var recData = RESTClient.Post(client, "evaluate/" + chromosome.Id, xmitData);

            if (recData != null) {
                var response = Binary.DeSerialize<Response>(recData);
                fitness = response.Result;

                Log.Debug(string.Format("Sending request to {0}: Response:{1}",
                                  client.BaseAddress, fitness));
            } else {
                throw new GAF.Network.SocketException("Data Packet was not received or was empty.");
            }

            return fitness;
        }

        #region Static Methods

        public void Dispose () {
            _pcQueue.Dispose();

            //close all available sockets
            _clientPool.Dispose();

        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the evaluations undertaken since the class was instantiated.
        /// </summary>
        /// <value>The evaluations.</value>
        public int Evaluations { get; private set; }

        #endregion

    }
}
